const https = require("https");
const http = require("http");
const getLists = require("./problem3.js");
const getCards = require("./problem4.js");

function getAllCards(boardId) {
  return getLists(boardId)
  .then((listsData) => {
    const listIds = listsData.map((list) => list.id);
    let promiseArray = listIds.map((listId) => getCards(listId));
    return Promise.all(promiseArray);
  })
}

module.exports = getAllCards;
