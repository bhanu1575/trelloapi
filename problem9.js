const https = require("https");
const http = require("http");
const { apiKey, apiToken } = require("./urls");
const options = {
  method: "PUT",
};

function getCheckItems(checklistID) {
  const url = `https://api.trello.com/1/checklists/${checklistID}/checkItems?key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    https.get(url, (response) => {
      let data = "";
      response.setEncoding("utf-8");
      response.on("data", (chunk) => {
        data += chunk;
      });

      response.on("end", () => {
        if (response.statusCode == 200) {
          resolve(JSON.parse(data));
        }
        const obj = {
          statusCode: response.statusCode,
          status: http.STATUS_CODES[response.statusCode],
          data: data.toString(),
        };
        reject(obj);
      });
    });
  });
}

function setStateOfCheckItem(cardId, checkItemId) {
  const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=complete&key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
      const request = https.request(url, options, (res) => {
        res.setEncoding("utf-8");
        let data = "";
        res.on("data", (chuck) => {
          data += chuck;
        });
        res.on("end", () => {
          if (res.statusCode == 200) {
            resolve(JSON.parse(data));
          }
          reject(data);
        });
      });
      request.on("error", (err) => {
        reject(err);
      });
      request.write(JSON.stringify(options));
      request.end();
  });
}

module.exports = {  getCheckItems , setStateOfCheckItem };
