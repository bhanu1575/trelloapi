const getBoard = require("../problem1.js");

const id = "9RCioOiR";

getBoard(id)
  .then((boardData) => {
    console.log(boardData);
  })
  .catch((err) => {
    console.log("Error caught");
    console.log(err);
  });
