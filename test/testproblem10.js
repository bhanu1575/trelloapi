const getAllCards = require("../problem5.js");
const { getCheckItems } = require("../problem9.js");
const setStateOfCheckItem = require('../problem10.js')

let obj = {};

getAllCards("9RCioOiR")
  .then((cardsData) => {
    cardsData = cardsData.flat();
    let promiseArrayOfCheckItems = [];

    cardsData.forEach((card) => {
      obj[card.id] = card.idChecklists;
      card.idChecklists.forEach((listId) => {
        promiseArrayOfCheckItems.push(getCheckItems(listId));
      });
    });

    return Promise.all(promiseArrayOfCheckItems);
  })
  .then((checkItemsData) => {
    checkItemsData = checkItemsData.flat();

    let result = checkItemsData.reduce((prevPromise, currentCheckItem) => {
      return prevPromise.then((res) => {
        console.log(res);
        let cardId = Object.keys(obj).find((cardId) => {
          return obj[cardId].includes(currentCheckItem.idChecklist);
        });
        return setStateOfCheckItem(cardId, currentCheckItem.id);
      });
    }, Promise.resolve());
    
    return result;
  })
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });
// 