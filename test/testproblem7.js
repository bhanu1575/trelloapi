const getLists = require("../problem3.js");
const deleteLists = require("../problem7.js");

const boardId = "N3LWouCJ";

getLists(boardId)
  .then((listsData) => {
    let promiseArray = listsData.map((list) => deleteLists(list.id));
    return Promise.all(promiseArray);
  })
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });
