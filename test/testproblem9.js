const getAllCards = require("../problem5.js");
const { getCheckItems, setStateOfCheckItem } = require("../problem9.js");

let obj = {};

getAllCards("9RCioOiR")
  .then((cardsData) => {
    cardsData = cardsData.flat();
    let promiseArrayOfgetCheckItems = [];

    cardsData.forEach((card) => {
      obj[card.id] = card.idChecklists;
      card.idChecklists.forEach((listId) => {
        promiseArrayOfgetCheckItems.push(getCheckItems(listId));
      });
    });

    return Promise.all(promiseArrayOfgetCheckItems);
  })
  .then((checkItemsData) => {
    checkItemsData = checkItemsData.flat();
    let promiseArray = checkItemsData.map((checkItem) => {
      let cardId = Object.keys(obj).find((id) => {
        return obj[id].includes(checkItem.idChecklist);
      });

      return setStateOfCheckItem(cardId, checkItem.id);
    });
    return Promise.all(promiseArray);
  })
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });
