const createBoard = require("../problem2.js");
const { createList, createCard } = require("../problem6.js");

createBoard("balaji")
  .then((boardData) => {
    console.log(boardData);
    let promiseArray = [];
    for (let index = 1; index <= 3; index++) {
      promiseArray.push(createList(`list-${index}`, boardData.id));
    }
    return Promise.all(promiseArray);
  })
  .then((listsData) => {
    console.log(listsData);
    let promiseArray = listsData.map((list, index) =>
      createCard(`card-${index + 1}`, list.id)
    );
    return Promise.all(promiseArray);
  })
  .then((cardsData) => {
    console.log(cardsData);
  })
  .catch((err) => {
    console.log("Error Caught");
    console.log(err);
  });
