const getLists = require('../problem3.js')
const deleteLists = require('../problem7.js')

const boardId = 'XpeqbYw4'


getLists(boardId)
.then((listsData)=>{
    let lists = listsData.map( list => list.id)

    let result = lists.reduce((prevPromise,currentlistId)=>{
        return prevPromise.then((res)=>{
            console.log((res));
            return deleteLists(currentlistId)
        })
    },Promise.resolve())

    return result
})
.then((res)=>{
    console.log(res);
})
.catch((err)=>{
  console.log(err);
})