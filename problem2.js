const https = require("https");
const { apiKey, apiToken } = require("./urls");
const options = {
  method: "POST",
};

function createBoard(name) {
  const url = `https://api.trello.com/1/boards/?name=${name}&key=${apiKey}&token=${apiToken}`;

  return new Promise((resolve, reject) => {
    let req = https.request(url, options, (res) => {
      res.setEncoding("utf-8");
      let data = "";

      res.on("data", (chunk) => {
        data += chunk;
      });
      res.on("end", () => {
        if (res.statusCode === 200) {
          resolve(JSON.parse(data));
        }
        reject(data);
      });
    });

    req.on("error", (error) => {
      reject(error);
    });
    req.write(JSON.stringify(options));
    req.end();
  });
}

module.exports = createBoard;
