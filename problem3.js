const https = require("https");
const http = require("http");
const { apiKey, apiToken } = require("./urls");

function getLists(id) {
  const url = `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    https.get(url, (response) => {
      let data = "";
      response.setEncoding("utf-8");
      response.on("data", (chunk) => {
        data = chunk;
      });

      response.on("end", () => {
        if (response.statusCode == 200) {
          resolve(JSON.parse(data));
        }
        const obj = {
          statusCode: response.statusCode,
          status: http.STATUS_CODES[response.statusCode],
          data: data,
        };
        reject(obj);
      });
    });
  });
}

module.exports = getLists;
