const https = require("https");
const http = require("http");
const { apiKey, apiToken } = require("./urls");

function getBoard(id) {
  const url = `https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    https.get(url, (response) => {
      let data = "";
      response.on("data", (chunk) => {
        data += chunk;
      });
      response.on("end", () => {
        if (response.statusCode == 200) {
          resolve(JSON.parse(data));
        }
        const obj = {
          statusCode: response.statusCode,
          status: http.STATUS_CODES[response.statusCode],
          data: data + " for Board",
        };
        reject(obj);
      });
    });
  });
}

module.exports = getBoard;
