const { apiKey, apiToken } = require("./urls");
const https = require("https");
const options = {
    method: "PUT",
  };
  
function setStateOfCheckItem(cardId, checkItemId) {
    const url = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=incomplete&key=${apiKey}&token=${apiToken}`;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const request = https.request(url, options, (res) => {
          res.setEncoding("utf-8");
          let data = "";
          res.on("data", (chuck) => {
            data += chuck;
          });
          res.on("end", () => {
            if (res.statusCode == 200) {
              resolve(JSON.parse(data));
            }
            reject(data);
          });
        });
        request.on("error", (err) => {
          reject(err);
        });
        request.write(JSON.stringify(options));
        request.end();
      }, 1000);
    });
  }


module.exports = setStateOfCheckItem