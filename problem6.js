
const https = require('https')
const {apiKey, apiToken} = require('./urls')
const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    }
}
function createList(name,boardId){
    const url = `https://api.trello.com/1/lists?name=${name}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`
    return new Promise((resolve,reject)=>{
        let req = https.request(url, options, (res)=>{
            res.setEncoding('utf-8')
            let data = ''
            res.on('data', (chunk) => {
              data += chunk
              
            });
            res.on('end', ()=>{
              if(res.statusCode === 200){
                resolve(JSON.parse(data))
              }
              reject(data)
          })
          })
      
          req.on('error', (error) => {
            reject(error)
          });
          req.write(JSON.stringify(options))
          req.end();
    })
}

function createCard(name,listId){
    const url = `https://api.trello.com/1/cards?name=${name}&idList=${listId}&key=${apiKey}&token=${apiToken}`
    return new Promise((resolve,reject)=>{
        let req = https.request(url, options, (res)=>{
            res.setEncoding('utf-8')
            let data = ''
            res.on('data', (chunk) => {
              data += chunk              
            });
            res.on('end', ()=>{
              if(res.statusCode === 200){
                resolve(JSON.parse(data))
              }
              reject(data)
          })
          })
      
          req.on('error', (error) => {
            reject(error)
          });
          req.write(JSON.stringify(options))
          req.end();
    })
}

module.exports = {createList,createCard}