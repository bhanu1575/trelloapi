const { apiKey, apiToken } = require("./urls.js");
const https = require("https");

const options = {
  method: "PUT",
};

function deleteLists(id) {
  const url = `https://api.trello.com/1/lists/${id}/closed?key=${apiKey}&token=${apiToken}&value=true`;
  return new Promise((resolve, reject) => {
    const req = https.request(url, options, (res) => {
      let data = "";
      res.setEncoding("utf-8");

      res.on("data", (chunk) => {
        data += chunk;
      });
      res.on("end", () => {
        resolve(JSON.parse(data));
      });
    });
    req.on("error", (err) => {
      reject(err);
    });
    req.write(JSON.stringify(options));
    req.end();
  });
}

module.exports = deleteLists;
